benchmark = 'nug21';
filename = ['[' benchmark ']average_Weight'];
M = csvread(['./Hybrid/Data/' filename]);
n = M(1:19800);
y = 1:19800;
plot(y, n)

%# horizontal line
hy = graph2d.constantline(0.5, 'Color',[.7 .7 .7]);
changedependvar(hy,'y');
ylim([0 1])

% Add a title with bold style
title(benchmark,... 
  'FontWeight','bold')