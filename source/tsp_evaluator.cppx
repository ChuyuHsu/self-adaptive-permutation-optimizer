#include <stdio.h>
#include <fstream>
#include <cmath>
#include <iostream>
#include <iomanip>
#include "global.h"
#include "evaluator.h"

TSP_Evaluator::TSP_Evaluator(){
	IEvaluator::init();
	
	vCityCoors.clear();
	problemSize = 0;
	best = 0.0;
}

TSP_Evaluator::TSP_Evaluator(char* fileName)
{
	IEvaluator::init();
	
	vCityCoors.clear();
	readData(fileName);	
	problemSize = vCityCoors.size();
	calcDistanceMatrix();
}

TSP_Evaluator::~TSP_Evaluator(){
	vCityCoors.clear();
}

bool TSP_Evaluator::readData(char* fileName){
	try
	{
		std::ifstream fin(fileName);
		if(!fin){
	        printf("ERROR: Failed to Read problem file.");
	        return false;
        }

		if(!fin.eof())
			fin >> this->best;
		while (!fin.eof())
		{
			CityCoor cc;
			fin >> cc.sn >> cc.x >> cc.y;
                        if(!vCityCoors.empty() && 
                           cc.sn == (vCityCoors.end()-1)->sn )
                           continue;

			vCityCoors.push_back(cc);
			// printf("n: %d, x:%f, y: %f \n", cc.sn, cc.x, cc.y);
		}
		fin.close();
		printf("%s loaded! \n", fileName);
		printf("Best: %f \n", this->best);
		printf("Problem size: %d \n", vCityCoors.size());
	}
	catch (...)
	{
	        printf("ERROR: Failed to Read problem file.");
		return false;
	}
	return true;	
}

double TSP_Evaluator::getBest(){
	return this->best;
}

int TSP_Evaluator::getProblemSize(){
	return this->problemSize;
}

double TSP_Evaluator::calc(const Chromosome *c)
{
	this->increaseNFE();
	double tmp = 0.0;
	
	for (int it=0; it<problemSize-1; ++it){
		int city1 = c->getVal(it);
		int city2 = c->getVal(it+1); 
		tmp += vDistanceMatrix[city1][city2];

		// printf("Distance(%d, %d): %f\n", city1+1, city2+1, vDistanceMatrix[city1][city2]);
	}
	int cityFirst = c->getVal(0);
	int cityLast = c->getVal(this->problemSize - 1);
	
	// printf("Distance(%d, %d): %f\n", cityLast+1, cityFirst+1, vDistanceMatrix[cityLast][cityFirst]);

	tmp += vDistanceMatrix[cityLast][cityFirst];
	
	// printf("Distance(TSP) = %f\n", tmp);
	return tmp;
}

void TSP_Evaluator::calcDistanceMatrix()
{
	double dltX, dltY;
	vDistanceMatrix.resize(this->problemSize);
	std::vector<std::vector<double> >::iterator it = vDistanceMatrix.begin();
	for(; it != vDistanceMatrix.end(); ++it)
		(*it).resize(problemSize);

	for (int i=0; i<problemSize; ++i)
	    for (int j=i; j<problemSize; ++j)
		if (i == j) vDistanceMatrix[i][j] = 0.0;
		else
		{
	       	     dltX = vCityCoors[i].x - vCityCoors[j].x;
	       	     // printf("(%d,%d): dltX = %f, ", i+1,j+1,dltX);
		     dltY = vCityCoors[i].y - vCityCoors[j].y;
	       	     // printf("dltY = %f, ",dltY);
		     vDistanceMatrix[i][j] = round(sqrt( dltX*dltX + dltY*dltY ));
		     vDistanceMatrix[j][i] = vDistanceMatrix[i][j]; 
	       	     // printf("Distance = %f\n", vDistanceMatrix[i][j]);
		}
}

//------------------------------------------------------------



TSPFixed_Evaluator::TSPFixed_Evaluator(char* filename)
: TSP_Evaluator(filename)
{
    this->best = this->best - 5.0;
}


double TSPFixed_Evaluator::calc(const Chromosome *c)
{
	double tmp = 0.0;
	tmp = TSP_Evaluator::calc(c);

	if(c->getVal(0) == 8)
	    tmp = tmp - 5;

	return tmp;
}
