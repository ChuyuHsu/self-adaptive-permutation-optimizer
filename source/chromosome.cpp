/***************************************************************************
 *   Copyright (C) 2004 by Tian-Li Yu                                      *
 *   tianliyu@cc.ee.ntu.edu.tw                                             *
 ***************************************************************************/

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include "global.h"
#include "chromosome.h"

Chromosome::Chromosome ()
{
    length = 0;
    gene.clear();
    evaluated = false;
}


Chromosome::Chromosome (int n_length)
{
   gene.clear();
    init (n_length, NULL);
}

Chromosome::Chromosome(int n_length, IEvaluator *eva)
{
    gene.clear();
    init(n_length, eva);
}


Chromosome::~Chromosome ()
{
}


void Chromosome::init (int n_length)
{
    length = n_length;

    gene.resize(n_length);
    evaluated = false;
    evaluator = NULL;
}


void Chromosome::init (int n_length, IEvaluator *eva)
{
    length = n_length;


    gene.resize(n_length);
    evaluated = false;
    evaluator = eva;
}

int Chromosome::getVal (int index) const
{
    if (index < 0 || index > length)
        outputErrMsg ("Index overrange in Chromosome::operator[]");

    return gene[index];
}


void Chromosome::setVal (int index, int val)
{
    if (index < 0 || index > length)
        outputErrMsg ("Index overrange in Chromosome::operator[]");

    gene.at(index) = val;
    evaluated = false;
}


double Chromosome::getFitness ()
{
    if (evaluated)
        return fitness;
    else
        return (fitness = evaluate ());
}


bool Chromosome::isEvaluated () const
{
    return evaluated;
}


double Chromosome::evaluate ()
{
	if(!evaluator)
		outputErrMsg ("Must have evaluator in Chromosome");

    evaluated = true;
	return evaluator->calc(this);

}


Chromosome & Chromosome::operator= (const Chromosome & c)
{
    this->clone(c);

    return *this;
}

void Chromosome::clone(const Chromosome& c)
{
    if (length != c.length) {
        length = c.length;
        this->gene.clear();
        init (length);
    }

    evaluated = c.evaluated;
    fitness = c.fitness;
    evaluator = c.evaluator;

    this->gene = c.gene;
}


void Chromosome::printf () const
{
    dump();
}


int Chromosome::getLength () const
{
    return length;
}


double Chromosome::getMaxFitness () const
{
    // For OneMax
    return ((double)length-1e-6);
}

void Chromosome::dump() const
{
	for(int i = 0; i < length; i++)
	{
		std::cout << std::setw(5) << gene[i];
	}
	std::cout << std::endl;
}

void Chromosome::setGenes(const int* t)
{
    if (length == 0)
        outputErrMsg ("Chromosome length can't be 0");
    for(int i = 0; i < length; i++)
    {
      setVal(i, t[i]);
    }
}
