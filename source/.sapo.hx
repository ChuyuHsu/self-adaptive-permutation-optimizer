#ifndef SAPO_H
#define SAPO_H

#include <vector>
#include <fstream>
#include "evaluator.h"
#include "chromosome.h"
#include "statistics.h"
#include "myrand.h"
#include "model.h"

class SAPO
{

    public:
        SAPO ();
        SAPO (int n_ell, int n_nInitial, int n_selectionPressure, int n_maxGen, int n_maxFe, IEvaluator *evaluator, char*);

        ~SAPO ();

        void init (int n_ell, int n_nInitial, int n_selectionPressure, int n_maxGen, int n_maxFe, IEvaluator *evaluator, char*);

        void initializePopulation ();
        void initializeModelBuilders ();
        void initializeOutputer (char*);
        void evaluate ();

        void selection ();

        /** tournament selection without replacement */
        void tournamentSelection ();
        
        void ModelBuilding ();
	void BaysianResample();
	void PursuitResample();
	void UCBResample();

        void replacePopulation ();

        void showStatistics ();
        void oneRun (bool output = true);
        int doIt (bool output = true);

        bool shouldTerminate ();
		
        Statistics stFitness;

    protected:

        int ell;                 // chromosome length
        int nInitial;            // initial population size
        int nCurrent;            // current population size
        int nNextGeneration;     // population size for the next generation
	int nSelection;
        int selectionPressure;

        Chromosome *population;
        Chromosome *offspring;
        Chromosome *modelPool;
        int *selectionIndex;
        int maxGen;
        int maxFe;
        int repeat;
        int fe;
        int generation;
        int bestIndex;
	IEvaluator *evaluator;
	std::vector<IModel*> ModelList;
	std::vector<double> ModelWeight;
	
	std::vector<std::ofstream*> ofileList;
	std::ofstream fin;

    private:
    	bool output;
};
#endif
