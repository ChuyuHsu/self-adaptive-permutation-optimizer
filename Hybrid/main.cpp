#include <string>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include "../source/statistics.h"
#include "hybrid.h"
#include "../source/chromosome.h"
#include "../source/global.h"

using namespace std;

int main (int argc, char *argv[])
{
    const clock_t begin_time = clock();
    if (argc != 9) {
        printf ("Hybrid populationSize maxFe repeat benchmark MARK CUT_POINT Problem R1\n");
        return -1;
    }

    int nInitial = atoi (argv[1]);
                                 // selection pressure
    int maxFe = atoi (argv[2]); // how many time to repeat
    int repeat = atoi (argv[3]); // how many time to repeat
    int bit = atoi(argv[4]);
    char *benchmark = argv[4];
    char *Mark = argv[5];
    NUM_CUTPOINTS = atoi(argv[6]);
    std::string Problem (argv[7]);
    R1 = atof(argv[8]);
    R2 = 1.0  - R1;
    int i;

    Statistics stGenS, stGenF, stFitness;
    int usedGen;

    int failNum = 0;
   
    char benchmarkLoc[100] = "../benchmarks/";
    strcat(benchmarkLoc, benchmark);
    
    printf("Benchmark: %s\n", benchmarkLoc);
    printf("Bratio: %f\n", Bratio); 
    
    IEvaluator *evaluator = NULL;
    if(Problem == "FSSP")   
    	evaluator = new FSSP_Evaluator(benchmarkLoc);
    else if(Problem == "LOP")
	    evaluator = new LOP_Evaluator(benchmarkLoc);
    else if(Problem == "QAP")    
	    evaluator = new QAP_Evaluator(benchmarkLoc);
    else if(Problem == "TSP")
	    evaluator = new TSP_Evaluator(benchmarkLoc);
    else if(Problem == "TSPF")
	    evaluator = new TSPFixed_Evaluator(benchmarkLoc);
    else if(Problem == "FLAT")
	    evaluator = new Flat_Evaluator(bit);
    else if(Problem == "CVRP")
      evaluator = new CVRP_Evaluator(benchmarkLoc);
    else{
	    printf("ERROR: Prblem invalid, %s", Problem.c_str());
      exit(1);
    }
    
    int ell = evaluator->getProblemSize();	

    for (i = 0; i < repeat; i++) {
      char MarkG[100];
      strcpy(MarkG, Mark);
      if(repeat > 1)
      {
          char run[50]; 
          sprintf(run,"_RUN%d",i+1);
          strcat(MarkG, run);
      }
      
      evaluator->init();

      Hybrid hybrid (ell, nInitial, maxFe, evaluator, MarkG);

      if (repeat == 1)
        usedGen = hybrid.doIt (true);
      else
        usedGen = hybrid.doIt (false);

      Chromosome ch(ell);
      

      if (hybrid.stFitness.getMin() - 0.01 > evaluator->getBest()) {
        printf ("-");
        failNum++;
        stGenF.record (usedGen);
      }
      else {
        printf ("+");
        stGenS.record (usedGen);
      }
      
      stFitness.record( hybrid.stFitness.getMin() );

      fflush (NULL);

    }
    

    printf ("\nAverage Gen of Success: %f\n", stGenS.getMean());
    printf ("Average Gen of Failure: %f\n", stGenF.getMean());
    printf ("Total number of Failure: %d\n", failNum);
	printf("Average Fitness: %f\n", stFitness.getMean());
    printf ("Execution time: %f\n", float(clock() - begin_time)/ CLOCKS_PER_SEC);

    FILE * pFile;
    char fileName[] = "Data/";
	strcat(fileName, argv[0]);
	strcat(fileName, "_");
	strcat(fileName, Mark);
	strcat(fileName, "_");
    strcat(fileName, argv[7]);
    pFile = fopen (fileName,"w");

    if(pFile)
    {
	    fprintf(pFile, "\nAverage Gen of Success: %f\n", stGenS.getMean());
	    fprintf(pFile, "Average Gen of Failure: %f\n", stGenF.getMean());
	    fprintf(pFile, "Total number of Failure: %d\n", failNum);
	    fprintf(pFile, "Average Fitness: %f\n", stFitness.getMean());
	    fprintf(pFile, "Problem Optima: %f\n", evaluator->getBest());
	    fprintf(pFile, "Execution time: %f\n", float(clock() - begin_time)/ CLOCKS_PER_SEC);
	    fclose (pFile);
    }

    delete evaluator;

    return EXIT_SUCCESS;
}
