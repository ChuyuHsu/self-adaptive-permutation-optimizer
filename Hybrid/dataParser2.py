#!/usr/bin/env python
import sys
import string
import os
import re
import csv

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

if len(sys.argv) == 1:
	print "No file name"
	sys.exit(1)

benchmark = sys.argv[1]
files = []
target_dirs = []
dirs = []

for num in range(1, 11):
	file_name = "./Data/" + benchmark + "_RUN" + str(num) + "/EHBSA-Weight.txt"
	dirs.append("./Data/" + benchmark + "_RUN" + str(num))
	target_dirs.append("./Data/archieve/" + benchmark + "_RUN" + str(num))
	files.append( file_name )
	#print file_name

weightArray = []
longest = 0
	
for file in files:
	if os.path.exists(file) == False:
		print "Experiments are still running"
		sys.exit(1)
		
	array = []
	fo = open(file, "r")
	for line in fo:
		length = 0
		for word in [x.strip() for x in line.split(',')]:
			# print word
			try:
				array.append(float(word))
			except:
				continue
			length = length + 1
			if length > longest:
				longest = length

	weightArray.append(array)
	fo.close()
	
divider = len(weightArray)
average_array = []
for i in range(longest):
	sum = 0.0
	local_divider = divider
	for j in range(divider):
		try:
			# print weightArray[j][i]
			sum += weightArray[j][i]
		except:
			local_divider = local_divider - 1
	average = sum / local_divider
	# print average
	average_array.append(average)
	
f = open("./Data/" + benchmark + 'average_Weight','w')
for a in average_array:
	f.write(str(a) + ",")
f.close


for idx, dir in enumerate(dirs):
	# print idx, target_dirs[idx]
    os.rename(dir, target_dirs[idx])
