#!/usr/bin/env python
import sys
import string
import os
import re

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

if len(sys.argv) == 1:
	print "No file name"
	sys.exit(1)

file_name_prefix = sys.argv[1]
files = [];

for num in range(0, 11):
	f_num = num / 10.0
	file_name = file_name_prefix + str(f_num)
	files.append( file_name )
	print file_name

avgGen = []
numFailure = []
avgFitness = []
optima = 0.0

for file in files:
	if os.path.exists(file) == False:
		print "Experiments are still running"
		sys.exit(1)
		
	fo = open(file, "r")
	content = fo.readlines()
	
	lineAvgGen = content[1]
	lineNumFailure = content[3]
	lineAvgFitness = content[4]
	
	
	m = re.search(': (.+?)\n', lineAvgGen)
	avgGen.append( m.group(1) )
	
	m = re.search(': (.+?)\n', lineNumFailure)
	numFailure.append( m.group(1) )
	
	m = re.search(': (.+?)\n', lineAvgFitness)
	avgFitness.append( m.group(1) )
	
	fo.close()
	
f = open(file_name_prefix + 'summary','w')
lineAvgGen = "Avg. Gen: "
for gen in avgGen:
	lineAvgGen = lineAvgGen + gen + ","
	
lineAvgFitness = "Avg. Fitness: "
for i in range(0, 11):
	lineAvgFitness += avgFitness[i]
	if numFailure[i] != "0":
		lineAvgFitness += "(" + numFailure[i] + ")"
	lineAvgFitness += ","
		
f.write(lineAvgGen + "\n")
f.write(lineAvgFitness + "\n")
	
f.close


for file in files:
    os.rename(file, "Temp/"+file)
