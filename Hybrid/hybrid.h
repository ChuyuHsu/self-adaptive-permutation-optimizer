#ifndef Hybrid_H
#define Hybrid_H

#include <vector>
#include <fstream>
#include "../source/evaluator.h"
#include "../source/chromosome.h"
#include "../source/statistics.h"
#include "../source/myrand.h"
#include "../source/model.h"


class Hybrid
{

    public:
        Hybrid ();
        Hybrid (int n_ell, int n_nInitial, int n_maxFe, IEvaluator *evaluator, char*);

        ~Hybrid ();

        void init (int n_ell, int n_nInitial, int n_maxFe, IEvaluator *evaluator, char*);

        void initializePopulation ();
        void initializeModelBuilders ();
        void initializeOutputer (char*);
        void evaluate();

        void selection();
        void tournamentSelection ();
        
        bool randomPickAndSample(IModel*);

        void ModelBuilding ();
        void PursuitSample ();
        void UCBSample();
        void UCBTunedSample();
        void DeterministicSample ();
        void BayesianSample ();
		void OneModelSample();
        void EnumerationSample();

        void showStatistics ();
        void oneRun (bool output = true);
        int doIt (bool output = true);

        bool shouldTerminate ();
		
        Statistics stFitness;

    protected:

        int ell;                 // chromosome length
        int nInitial;            // initial population size
        int nCurrent;            // current population size
        int nNextGeneration;     // population size for the next generation
        int selectionPressure;

        Chromosome *population;


        int maxFe;
        int repeat;
        int fe;
        int generation;
        int bestIndex;
        IEvaluator *evaluator;
        std::vector<IModel*> ModelList;
        std::vector<double> ModelWeight;
        
        std::vector<Statistics> UCB_mean;
        std::vector<int> UCB_times;
        int UCB_total_times;
        
        std::vector<std::ofstream*> ofileList;
        std::ofstream fin;

    private:
    	  bool output;
		    MyRand random;
};
#endif
