#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <sys/stat.h> 
#include <ctime>
#include <cstring>

#include "hybrid.h"
#include "../source/global.h"
#include "../source/statistics.h"
#include "../source/myrand.h"
#include "../source/mkpath.h"
#include "../source/model.h"
#include "../source/model_EHBSA.h"
#include "../source/model_NHBSA.h"
#include "../source/model_Hybrid.h"
#include "../source/Model_PlackettLuce.h"
#include "../source/model_order.h"
#include "../source/dice.h"


Hybrid::Hybrid ()
{
    ell = 0;
    nInitial = 0;
    nCurrent = 0;
    fe = 0;

    nNextGeneration = 0;

    maxFe = -1;

    population = NULL;

	
    evaluator = NULL;
    ModelList.clear();
    ofileList.clear();

}


Hybrid::Hybrid (int n_ell, int n_nInitial, int n_maxFe, IEvaluator *evaluator, char *pos)
{
    init (n_ell, n_nInitial, n_maxFe, evaluator, pos);
}


Hybrid::~Hybrid ()
{
    for(int i = 0; i < (int)ofileList.size(); i++){
	    ofileList[i]->close();
	    delete ofileList[i];
    }
    
    for(int i = 0; i < (int)ModelList.size(); i++){
		  delete ModelList[i];
    }
    
	
    delete[] population;
}


void
Hybrid::init (int n_ell, int n_nInitial, int n_maxFe, IEvaluator *_evaluator, char* pos)
{
    int i;

    ell = n_ell;
    nInitial = n_nInitial;
    nCurrent = nInitial;
    maxFe = n_maxFe;
	  evaluator = _evaluator;

    population = new Chromosome[nInitial];


    for (i = 0; i < nInitial; i++) {
        population[i].init (ell, evaluator);
    }

	  initializePopulation ();
	  initializeModelBuilders ();
	  initializeOutputer(pos);
}

/* Initial the permutations */
void Hybrid::initializePopulation ()
{
    for (int i = 0; i < nInitial; i++)
	{
		int* temp = new int[ell];
		myRand.uniformArray(temp, ell, 0, ell-1);
			
        for (int j = 0; j < ell; j++)
		{
			population[i].setVal(j, temp[j]);
		}
		delete[] temp;
	}
}


void Hybrid::initializeOutputer(char* _pos){
	char pos[100] = "./Data/";
	// time_t rawtime;
	// struct tm * timeinfo;
	// time (&rawtime);
	// timeinfo = localtime (&rawtime);	
	// strftime (pos,100,"./Data/%Y%m%d/",timeinfo);

	if(_pos != NULL)
		strcat(pos, _pos);
	
	if(MKPATH(pos, S_IEXEC | S_IWRITE | S_IREAD) != 0)
	{
		perror("mkdir() error");
		printf("ERROR: Can't make dir.\n");
		exit(1);
	}
	
	ofileList.clear();
	// for(int i = 0; i < (int)ModelList.size(); i++){
		// char filename[256];

		// strcpy(filename, pos);
		// strcat(filename, "/");
		// strcat(filename, (*ModelList[i]).getModelName());
		// strcat(filename, ".txt");
		
		// std::ofstream* pin = new std::ofstream();
		// pin->open(filename, std::ofstream::out | std::ofstream::app);
		// ofileList.push_back(pin);
	// }
	
	for(int i = 0; i < (int)ModelList.size(); i++){
		char filename[256];
		
		strcpy(filename, pos);
		strcat(filename, "/");
		strcat(filename, (*ModelList[i]).getModelName());
		strcat(filename, "-Weight.txt");
		
		std::ofstream* pin = new std::ofstream();
		pin->open(filename, std::ofstream::out | std::ofstream::app);
		ofileList.push_back(pin);
	}

	// char filename[256];
	// strcpy(filename, pos);
	// strcat(filename, "/fitness.txt");
	// std::ofstream* pin = new std::ofstream();
	// pin->open(filename, std::ofstream::out | std::ofstream::app);
	// ofileList.push_back(pin);
}


void Hybrid::initializeModelBuilders()
{
	ModelList.clear();
	
	// ModelList.push_back( new Model_PlackettLuce(ell, population, nInitial) );
	// ModelList.push_back( new Model_Order(ell, population, nInitial) );
	 ModelList.push_back( new Model_EHBSA(ell, population, nInitial) );
	 ModelList.push_back( new Model_NHBSA(ell, population, nInitial) );
	// ModelList.push_back( new Model_Hybrid(R1, R2, ell, population, nInitial) );
	
	// ModelList.push_back( new Model_EHBSAwoTemplate(ell, population, nInitial) );
	// ModelList.push_back( new Model_NHBSAwoTemplate(ell, population, nInitial) );
	
	int size = ModelList.size();
	ModelWeight.resize(size);
    UCB_mean.resize(size);
    UCB_times.resize(size);
    UCB_total_times = 0;
	for(int i=0; i < size; i++){
		ModelWeight[i] = (double)1 / (double)size;
        UCB_mean.at(i).reset();
        UCB_times.at(i) = 0;
	}
}



/**
true: replaced, 
false: original
*/
bool Hybrid::randomPickAndSample(IModel* model)
{
  int t = random.uniformInt(0, nInitial-1);
  // population[t].dump();
  Chromosome child = model->resampleWithTemplate(&population[t]);   
  // child.dump();
  if(child.getFitness() < population[t].getFitness())
  {
	population[t].clone(child);
    return true;
  }
  else{
    return false;
  }
  return false;
}

void Hybrid::ModelBuilding()
{	
	// printf("model building, ");

	for(int i = 0; i < (int)ModelList.size(); i++)
	{
	    IModel *m = ModelList.at(i);
	    m->build();
	
	   // m->dump();	
	}
}


void Hybrid::DeterministicSample(){
    double Beta = 0.01; 
    int numModels = (int)ModelList.size();

	  int templateIndex = random.uniformInt(0, nInitial-1);
    int bestModel = -1;
    double best = population[templateIndex].getFitness();
    
	

    for(int i = 0; i < numModels; i++)
    {
      IModel *m = ModelList.at(i);
      Chromosome child = m->resampleWithTemplate(&population[templateIndex]);
      if(child.getFitness() < best)
      {
        best = child.getFitness();
        bestModel = i;
        population[templateIndex].clone(child);
      }
    }

    //pursuit part

	for(int i = 0; i < numModels; i++)
	{
		if(bestModel != -1)
		{
			if(i == bestModel)
			{
				ModelWeight[i] = (1 - Beta) * ModelWeight[i] + Beta;
			}
			else
			{
				ModelWeight[i] = (1 - Beta) * ModelWeight[i];
			}
		}

		(*ofileList[i]) << ModelWeight[i] << ",";	
		if(output)
			printf("Weight[%s]: %f\n", ModelList[i]->getModelName(), ModelWeight[i]);
	
	}
}


void Hybrid::OneModelSample()
{
    /*always pick one of models to sample new individual*/
    int modelIndex = 0;

    int templateIndex = random.uniformInt(0, nInitial-1);
    IModel *m = ModelList.at(modelIndex);
    //m->dump();
    Chromosome newC = m->resampleWithTemplate(&population[templateIndex]);

    if(newC.getFitness() <= population[templateIndex].getFitness())
    {
        population[templateIndex].clone(newC);
    }

}

void Hybrid::EnumerationSample()
{
    int numModels = (int)ModelList.size();
    int t = random.uniformInt(0, nInitial-1);
    double best_value = DBL_MAX;
    Chromosome best;

    for (int i = 0; i < numModels; i++) {
        IModel *m = ModelList.at(i);
        
        Chromosome c = m->resampleWithTemplate(&population[t]);
        if(c.getFitness() <= best_value)
        {
           best_value = c.getFitness();
           best.clone(c);
        }
    }

    if(best.getFitness() <= population[t].getFitness())
        population[t].clone(best);
}


void Hybrid::BayesianSample()
{
	// double totalScore = 0.0;
	// double *scoreArray = new  double[ModelList.size()];
	// int numModels = (int)ModelList.size();
	 
	// for(int i = 0; i < numModels; i++)
	// {
	    
		// IModel *m = ModelList.at(i);
		// scoreArray[i] =
		  // m->getProbabilityOfModel(population, nInitial);
		// totalScore += scoreArray[i];

	// }
	
	
	// for(int i = 0; i < numModels; i++)
	// {
		// double  p = ((totalScore - scoreArray[i]) / totalScore);
		// if(totalScore == 0.0) 
		    // p = 0.5;


		// if(output)
			// printf("Weight[%s]: %f (%f)\n", ModelList[i]->getModelName(), p, scoreArray[i]);
	// }
	

	
	// for(int k = 0; k < 1; k++)
	// {
      // Dice dice(scoreArray, numModels);
      // int modelIndex = dice.sample();

      // this->randomPickAndSample(ModelList.at(modelIndex));
	// }
	

	
	// delete[] scoreArray;
}

void Hybrid::UCBSample(){
  int numModels = (int) ModelList.size();
  //printf("UCB Smaple\n");
  if(generation == 0)
  {
    for(int i = 0; i < numModels; i++)
    {
      IModel *m = ModelList.at(i);
      UCB_times[i]++;
      UCB_total_times++;
      if( this->randomPickAndSample(m) == true)
      {
        UCB_mean[i].record(1.0);
      }else
      {
        UCB_mean[i].record(0.0);
      }
    }
  }else
  {
    //find the maximun UCB
    int index = -1;
    double max = -DBL_MAX;
    for(int i = 0; i < numModels; i++)
    {
       double ucb = 
         UCB_mean[i].getMean() + sqrt( 2*log(UCB_total_times) / UCB_times[i] );
       if(ucb > max)
       {
         max = ucb;
         index = i;
       }
    }
    //printf("UCB Arm: %d (%f)\n", index, max);
  
    IModel *m = ModelList.at(index);
    UCB_total_times++;
    UCB_times[index]++;
    // m->dump();

    if(this->randomPickAndSample(m) == true)
    {
      UCB_mean[index].record(1.0);
    }
    else
    {
      UCB_mean[index].record(0.0);  
    }

  }
}

void Hybrid::UCBTunedSample()
{
  int numModels = (int) ModelList.size();
  //printf("UCB Smaple\n");
  if(generation == 0)
  {
    for(int i = 0; i < numModels; i++)
    {
      IModel *m = ModelList.at(i);
      UCB_times[i]++;
      UCB_total_times++;
      if( this->randomPickAndSample(m) == true)
      {
        UCB_mean[i].record(1.0);
      }else
      {
        UCB_mean[i].record(0.0);
      }
    }
  }else
  {
    //find the maximun UCB
    int index = -1;
    double max = -DBL_MAX;
    for(int i = 0; i < numModels; i++)
    {
       double v = UCB_mean[i].getVariance() + sqrt(2*log(UCB_total_times) / (double)UCB_times[i]);
       double V = (v > 0.25)? 0.25 : v;
       double ucb = 
         UCB_mean[i].getMean() + sqrt( 2*log(UCB_total_times) / (double)UCB_times[i] * V);
       if(ucb > max)
       {
         max = ucb;
         index = i;
       }
    }
    //printf("UCB Arm: %d (%f)\n", index, max);
  
    IModel *m = ModelList.at(index);
    UCB_total_times++;
    UCB_times[index]++;

    if(this->randomPickAndSample(m) == true)
    {
      UCB_mean[index].record(1.0);
    }
    else
    {
      UCB_mean[index].record(0.0);  
    }

  }
}

void Hybrid::PursuitSample(){
  double Beta = 0.01; 
  int numModels = (int)ModelList.size();

  Dice dice(ModelWeight);
  int modelIndex = dice.sample();
	
	IModel *m = ModelList.at(modelIndex);

  if(this->randomPickAndSample(m) == true)
  {
    for(int i = 0; i < numModels; i++)
    {
      if(i == modelIndex)
        ModelWeight[i] = (1-Beta) * ModelWeight[i] + Beta;
      else
        ModelWeight[i] = (1-Beta) * ModelWeight[i];
    }
  }

    //pursuit part
	for(int i = 0; i < numModels; i++)
	{
		(*ofileList[i]) << ModelWeight[i] << ",";	
		if(output)
			printf("Weight[%s]: %f\n", ModelList[i]->getModelName(), ModelWeight[i]);
	
	}
}


void Hybrid::showStatistics ()
{
    printf ("Gen:%d  Fitness:(StD/Max/Mean/Min):%f/%f/%f/%f Chromosome Length:%d\n",
        generation, stFitness.getStdev(),stFitness.getMax (), stFitness.getMean (),
        stFitness.getMin (), population[0].getLength ());
    printf ("best chromosome:");
    population[bestIndex].printf();
    // printf ("\n");
}




void Hybrid::oneRun (bool _output)
{
    int i;
    output = _output;

    ModelBuilding();
    //DeterministicSample();
    //BayesianResample();
    //PursuitSample();
    // OneModelSample();
    EnumerationSample();
    //UCBSample();

    double min = DBL_MAX;
    stFitness.reset ();
    for (i = 0; i < nCurrent; i++) {
        double fitness = population[i].getFitness ();
        if (fitness < min) {
            min = fitness;
            bestIndex = i;
        }
        stFitness.record (fitness);
    }

    if (output)
        showStatistics ();

    // (*ofileList.back()) << stFitness.getMin () << ",";
   // ofileList.back()->flush();

    generation++;
}


int Hybrid::doIt (bool output)
{
    generation = 0;

    while (!shouldTerminate ()) {
        oneRun (output);
    }
    
    //(*ofileList.back()) << stFitness.getMin () << ",";
    //ofileList.back()->flush();
    
    return generation;
}


bool Hybrid::shouldTerminate ()
{

  bool termination = false;

    // Reach maximal # of function evaluations
    if (maxFe != -1) {
       if (evaluator->getNFE() >= (unsigned int)maxFe)
            termination = true;
    }

    // Found a satisfactory solution
    // if (stFitness.getMax() >= population[0].getMaxFitness())
        // termination = true;

    // The population loses diversity
    if (stFitness.getMax()-1e-6 < stFitness.getMean())
        termination = true;
		
    if (stFitness.getMin()-0.001 <= evaluator->getBest())
	termination = true;

    return termination;

}
