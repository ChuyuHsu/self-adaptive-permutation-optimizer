/***************************************************************************
 *   Copyright (C) 2004 by Tian-Li Yu                                      *
 *   tianliyu@cc.ee.ntu.edu.tw                                             *
 *                                                                         *
 *   You can redistribute it and/or modify it as you like                  *
 ***************************************************************************/
#include <string>
#include <cmath>
#include <cstdio>
#include <cfloat>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>

#include "../../source/chromosome.h"
#include "../../source/evaluator.h"
#include "../../source/global.h"
#include "../../source/myrand.h"
#include "../../source/model.h"

using namespace std;

void TournamentSelection ();

int ell = 0;
int populationSize = 0;
Chromosome* offspring = NULL;
Chromosome* population = NULL;

int main (int argc, char *argv[])
{

    if (argc != 4) {
        printf ("B_TEST populationSize repeat benchmark\n");
        return -1;
    }

    populationSize = atoi(argv[1]);
    int repeat = atoi(argv[2]);
    int bit = atoi(argv[3]);
    char *benchmark = argv[3];

    char benchmarkLoc[100] = "../../benchmarks/";
    strcat(benchmarkLoc, benchmark);
    
    printf("Benchmark: %s\n", benchmarkLoc);
    
	// IEvaluator *evaluator = new Flat_Evaluator(bit);
	IEvaluator *evaluator = new TSP_Evaluator(benchmarkLoc);

    ell = evaluator->getProblemSize(); 

    population = new Chromosome[populationSize];
    offspring = new Chromosome[populationSize];
    MyRand myRand;    
    
    vector<IModel*> ModelList;
    ModelList.push_back( new Model_EHBSA(ell) );
    ModelList.push_back( new Model_NHBSA(ell) );

    ofstream fout[2];
    fout[0].open("out1.txt", ofstream::out);
    fout[1].open("out2.txt", ofstream::out);

    for(int x = 0; x < repeat; x++){
		for(int i = 0; i < populationSize; i++)
		{
			population[i].init(ell, evaluator);
			offspring[i].init(ell, evaluator);
		}


		for (int i = 0; i < populationSize; i++)
		{
			int* temp = new int[ell];
			myRand.uniformArray(temp, ell, 0, ell-1);
				
			for (int j = 0; j < ell; j++)
			{
			population[i].setVal(j, temp[j]);
			}
		   // population[i].dump();
			delete[] temp;
		}
	
		double Score[2][2]; 

		printf("Score for first chromosome (initialization):\n");
		for(int i = 0; i < (int)ModelList.size(); i++)
		{

			IModel *m = ModelList.at(i);
			m->buildModel(population, populationSize);
			double score = m->getFitnessOfModel(population, 1);
			printf("%f \n", score);
			fout[i] << score << ",";
			Score[0][i] = score;
		}
		
		TournamentSelection();
		
		printf("\nScore for first chromosome (selection):\n");
		for(int i = 0; i < (int)ModelList.size(); i++)
		{

			IModel *m = ModelList.at(i);
			m->buildModel(offspring, populationSize/2);
			double score = m->getFitnessOfModel(offspring, 1);
			printf("%f \n", score);
			fout[i] << score << ",";
			Score[1][i] = score;
		}
		
		


    }

    fout[0].close();
    fout[1].close();

    delete evaluator;   
    delete[] population;
    return EXIT_SUCCESS;
}


void TournamentSelection ()
{
    int i, j;

    // Adjusting population size 
	int selectionPressure = 2;
    int nNextGeneration = populationSize;
    int nCurrent = populationSize;
	int nSelection = nNextGeneration / selectionPressure;


    int randArray[selectionPressure * nNextGeneration];

    int q = (selectionPressure * nNextGeneration) / nCurrent;
    int r = (selectionPressure * nNextGeneration) % nCurrent;

	MyRand myRand;
    for (i = 0; i < q; i++)
        myRand.uniformArray (randArray + (i * nCurrent), nCurrent, 0, nCurrent - 1);

    myRand.uniformArray (randArray + (q * nCurrent), r, 0, nCurrent - 1);

    for (i = 0; i < nSelection; i++) {

        int winner = 0;
		double winnerFitness = DBL_MAX;

        for (j = 0; j < selectionPressure; j++) {
            int challenger = randArray[selectionPressure * i + j];
            double challengerFitness = population[challenger].getFitness ();

            if (challengerFitness < winnerFitness) {
                winner = challenger;
                winnerFitness = challengerFitness;
            }

        }
    	offspring[i] = population[winner];	
    }
}
