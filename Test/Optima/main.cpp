/***************************************************************************
 *   Copyright (C) 2004 by Tian-Li Yu                                      *
 *   tianliyu@cc.ee.ntu.edu.tw                                             *
 *                                                                         *
 *   You can redistribute it and/or modify it as you like                  *
 ***************************************************************************/
#include <string>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>

#include "../../source/chromosome.h"
#include "../../source/evaluator.h"
#include "../../source/global.h"

using namespace std;

void readSolution(int *);

int ell = 0;

int main (int argc, char *argv[])
{

    if (argc != 2) {
        printf ("TEST benchmark\n");
        return -1;
    }

    char *benchmark = argv[1];

    char benchmarkLoc[100] = "../../benchmarks/tsp/";
    strcat(benchmarkLoc, benchmark);
    
    printf("Benchmark: %s\n", benchmarkLoc);
    
    IEvaluator *evaluator = new TSP_Evaluator(benchmarkLoc);

    ell = evaluator->getProblemSize(); 
    Chromosome c(ell, evaluator);

    int *optSolution = new int[ell];
    readSolution(optSolution);

    c.setGenes(optSolution);

    double fitness = c.getFitness();

    printf("Benchmark: %s, Optimal fitness: %f\n", benchmark, fitness);


    delete[] optSolution;
    return EXIT_SUCCESS;
}

void readSolution(int* array)
{
   ifstream fin("opt.txt");
   if(!fin){
      printf("ERROR: Cann't find opt.txt\n");
      exit(1);
   }

   for(int i = 0; i < ell; i++)
   {
      /*if(i > ell){
         printf("ERROR: Solution doesn't match the problem benchmark\n");
         exit(1);
      }*/

      int j;
      fin >> j;
      array[i] = j-1;
      printf("%d ", j);
   }

   printf("\n");
   fin.close();   
}
