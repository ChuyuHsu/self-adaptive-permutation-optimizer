/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>

#include "../../source/evaluator.h"
#include "../../source/chromosome.h"

using namespace std;

class TestCVRP_Evaluator : public CppUnit::TestFixture {
private:
	CVRP_Evaluator *testCVRP_Evaluator;
	string benchmark1, benchmark2, benchmark3, optima1, optima2, optima3;
public:
	TestCVRP_Evaluator() : testCVRP_Evaluator(NULL) {
		benchmark1 = "../../benchmarks/cvrp/A-n32-k5.vrp";
	}
	virtual ~TestCVRP_Evaluator() {
		delete testCVRP_Evaluator;
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for CVRP_Evaluator");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestCVRP_Evaluator>("Test1 - Constructor.",
				&TestCVRP_Evaluator::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestCVRP_Evaluator>("Test2 - Attributes.",
				&TestCVRP_Evaluator::testAttributes ));
				
		suiteOfTests->addTest(new CppUnit::TestCaller<TestCVRP_Evaluator>("Test3 - Calculate.",
				&TestCVRP_Evaluator::testCalculate ));
                
		suiteOfTests->addTest(new CppUnit::TestCaller<TestCVRP_Evaluator>("Test3 - Calculate2.",
				&TestCVRP_Evaluator::testCalculate2 ));
                
		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( testCVRP_Evaluator == NULL );
		
		testCVRP_Evaluator = new CVRP_Evaluator(benchmark1.c_str());
		
		CPPUNIT_ASSERT( testCVRP_Evaluator != NULL );
	}
	
	void testAttributes()
	{
			printf("test Attributes\n");
			testCVRP_Evaluator = new CVRP_Evaluator(benchmark1.c_str());
			
			printf("p-size: %d \n", testCVRP_Evaluator->getProblemSize());
			printf("best: %f \n", testCVRP_Evaluator->getBest());
			
			CPPUNIT_ASSERT( testCVRP_Evaluator->getProblemSize() == 35 );
			CPPUNIT_ASSERT( testCVRP_Evaluator->getBest() == 784 );
	}
    
	void testCalculate()
	{
        printf("test Calculate\n");
        testCVRP_Evaluator = new CVRP_Evaluator(benchmark1.c_str());
        
        int opt[] = {20,30,18,16,12,6,25,31,
                    11,0,15,29,32,
                    26,23,33,
                    28,17,7,8,21,14,9,24,4,19,34,
                    13,27,10,3,22,2,1,5};
        
        Chromosome c(35);
        c.setGenes(opt);
        // c.dump();
        CPPUNIT_ASSERT( testCVRP_Evaluator->calc(&c) == 784 );
	}
    
	void testCalculate2()
	{
        printf("test Calculate2\n");
        testCVRP_Evaluator = new CVRP_Evaluator(benchmark1.c_str());
        
        int opt[] = {13, 34,
                    29,17,28,5,32,
                    33,
                    31,
                    27,24,18,21,22,4,10,23,12,16,3,1,14,19,6,15,2,30,25,7,20,26,11,9,8};
        
        Chromosome c(35);
        c.setGenes(opt);
        // c.dump();
        // printf("%f", testCVRP_Evaluator->calc(&c));
        CPPUNIT_ASSERT( testCVRP_Evaluator->calc(&c) >= 6516 - 0.5 );
	}
};
