/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>

#include "../../source/evaluator.h"
#include "../../source/chromosome.h"

using namespace std;

class TestQAP_Evaluator : public CppUnit::TestFixture {
private:
	QAP_Evaluator *testQAP_Evaluator;
	string benchmark1, benchmark2, benchmark3, optima1, optima2, optima3;
public:
	TestQAP_Evaluator() : testQAP_Evaluator(NULL) {
		benchmark1 = "../../benchmarks/qap/bur26a.dat";
	}
	virtual ~TestQAP_Evaluator() {
		delete testQAP_Evaluator;
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for QAP_Evaluator");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestQAP_Evaluator>("Test1 - Constructor.",
				&TestQAP_Evaluator::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestQAP_Evaluator>("Test2 - Attributes.",
				&TestQAP_Evaluator::testAttributes ));
				
		// suiteOfTests->addTest(new CppUnit::TestCaller<TestQAP_Evaluator>("Test5 - Calculation3.",
				// &TestQAP_Evaluator::testCalculation3 ));

		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( testQAP_Evaluator == NULL );
		
		testQAP_Evaluator = new QAP_Evaluator(benchmark1.c_str());
		
		CPPUNIT_ASSERT( testQAP_Evaluator != NULL );
	}
	
	void testAttributes()
	{
		printf("test Attributes\n");
		testQAP_Evaluator = new QAP_Evaluator(benchmark1.c_str());
		
		printf("p-size: %d \n", testQAP_Evaluator->getProblemSize());
		printf("best: %f \n", testQAP_Evaluator->getBest());
		
		//testQAP_Evaluator->dump();
		
		CPPUNIT_ASSERT( testQAP_Evaluator->getProblemSize() == 26 );
		CPPUNIT_ASSERT( testQAP_Evaluator->getBest() == 5426670.0 );
		
		
	}

};
