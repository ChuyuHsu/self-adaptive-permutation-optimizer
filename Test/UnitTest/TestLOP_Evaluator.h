/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>

#include "../../source/evaluator.h"
#include "../../source/chromosome.h"

using namespace std;

class TestLOP_Evaluator : public CppUnit::TestFixture {
private:
	LOP_Evaluator *testLOP_Evaluator;
	string benchmark1, benchmark2, benchmark3, optima1, optima2, optima3;
public:
	TestLOP_Evaluator() : testLOP_Evaluator(NULL) {
		benchmark1 = "../../benchmarks/lop/lolib/t75i11xx";
	}
	virtual ~TestLOP_Evaluator() {
		delete testLOP_Evaluator;
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for LOP_Evaluator");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestLOP_Evaluator>("Test1 - Constructor.",
				&TestLOP_Evaluator::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestLOP_Evaluator>("Test2 - Attributes.",
				&TestLOP_Evaluator::testAttributes ));
				
		// suiteOfTests->addTest(new CppUnit::TestCaller<TestLOP_Evaluator>("Test5 - Calculation3.",
				// &TestLOP_Evaluator::testCalculation3 ));

		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( testLOP_Evaluator == NULL );
		
		testLOP_Evaluator = new LOP_Evaluator(benchmark1.c_str());
		
		CPPUNIT_ASSERT( testLOP_Evaluator != NULL );
	}
	
	void testAttributes()
	{
			printf("test Attributes\n");
			testLOP_Evaluator = new LOP_Evaluator(benchmark1.c_str());
			
			// printf("p-size: %d \n", testLOP_Evaluator->getProblemSize());
			printf("best: %f \n", testLOP_Evaluator->getBest());
			
			CPPUNIT_ASSERT( testLOP_Evaluator->getProblemSize() == 44 );
			CPPUNIT_ASSERT( testLOP_Evaluator->getBest() == -72664466.0 );
	}
};
