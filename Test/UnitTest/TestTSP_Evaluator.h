/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>

#include "../../source/evaluator.h"
#include "../../source/chromosome.h"

using namespace std;

class TestTSP_Evaluator : public CppUnit::TestFixture {
private:
	TSP_Evaluator *testTSP_Evaluator;
	string benchmark1, benchmark2, benchmark3, optima1, optima2, optima3;
public:
	TestTSP_Evaluator() : testTSP_Evaluator(NULL) {
		benchmark1 = "../../benchmarks/tsp/eil51.xml";
		benchmark2 = "../../benchmarks/tsp/brg180.xml";
	}
	virtual ~TestTSP_Evaluator() {
		delete testTSP_Evaluator;
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for TSP_Evaluator");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestTSP_Evaluator>("Test1 - Constructor.",
				&TestTSP_Evaluator::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestTSP_Evaluator>("Test2 - Attributes.",
				&TestTSP_Evaluator::testAttributes ));
				
		suiteOfTests->addTest(new CppUnit::TestCaller<TestTSP_Evaluator>("Test3 - Attributes2.",
				&TestTSP_Evaluator::testAttributes2 ));
		// suiteOfTests->addTest(new CppUnit::TestCaller<TestTSP_Evaluator>("Test5 - Calculation3.",
				// &TestTSP_Evaluator::testCalculation3 ));

		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( testTSP_Evaluator == NULL );
		
		testTSP_Evaluator = new TSP_Evaluator(benchmark1.c_str());
		
		CPPUNIT_ASSERT( testTSP_Evaluator != NULL );
	}
	
	void testAttributes()
	{
			printf("test Attributes\n");
			testTSP_Evaluator = new TSP_Evaluator(benchmark2.c_str());
			
			// printf("p-size: %d \n", testTSP_Evaluator->getProblemSize());
			// printf("best: %f \n", testTSP_Evaluator->getBest());
			
			CPPUNIT_ASSERT( testTSP_Evaluator->getProblemSize() == 180 );
			CPPUNIT_ASSERT( testTSP_Evaluator->getBest() == 1950.0 );
			
			// delete testTSP_Evaluator;
	}

	void testAttributes2()
	{
			printf("test Attributes2\n");
			testTSP_Evaluator = new TSP_Evaluator(benchmark1.c_str());
			
			// printf("p-size: %d \n", testTSP_Evaluator->getProblemSize());
			// printf("best: %f \n", testTSP_Evaluator->getBest());
			
			CPPUNIT_ASSERT( testTSP_Evaluator->getProblemSize() == 51 );
			CPPUNIT_ASSERT( testTSP_Evaluator->getBest() == 426.0 );
			
			// delete testTSP_Evaluator;
	}
};
