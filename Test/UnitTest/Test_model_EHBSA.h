/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>
#include <cmath>


#include "../../source/model.h"
#include "../../source/model_EHBSA.h"
#include "../../source/chromosome.h"

using namespace std;

class Test_Model_EHBSA : public CppUnit::TestFixture {
private:
	Model_EHBSA *model;
public:
	Test_Model_EHBSA() : model(NULL) {
	}
	virtual ~Test_Model_EHBSA() {
		delete model;
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for Model_EHBSA");

		suiteOfTests->addTest(new CppUnit::TestCaller<Test_Model_EHBSA>("Test1 - Constructor.",
				&Test_Model_EHBSA::testConstructor ));

		suiteOfTests->addTest(new CppUnit::TestCaller<Test_Model_EHBSA>("Test2 - Probability1.",
				&Test_Model_EHBSA::testProbability1 ));

		suiteOfTests->addTest(new CppUnit::TestCaller<Test_Model_EHBSA>("Test3 - Probability2.",
				&Test_Model_EHBSA::testProbability2 ));			

		suiteOfTests->addTest(new CppUnit::TestCaller<Test_Model_EHBSA>("Test4 - Probability3.",
				&Test_Model_EHBSA::testProbability3 ));		
				
		 suiteOfTests->addTest(new CppUnit::TestCaller<Test_Model_EHBSA>("Test5 - Model Gain.",
				 &Test_Model_EHBSA::testModelGain ));
		 
		 suiteOfTests->addTest(new CppUnit::TestCaller<Test_Model_EHBSA>("Test6 - Update.",
				 &Test_Model_EHBSA::testUpdate ));

		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	#define ELL3 3
	void testConstructor() 
	{
		model = new Model_EHBSA(ELL3);
		CPPUNIT_ASSERT( model != NULL );
	}
	
	
	void testProbability1()
	{
		cout << "testProbability1\n";
		
		model = new Model_EHBSA(ELL3);
		
		Chromosome* population = new Chromosome[6];
		
		
		int a[6][3] = {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}};
		
		for(int i = 0; i < 6; i++)
		{
			population[i].init (ELL3);
			population[i].setGenes(a[i]);
		}
		
		model->buildModel(population, 6);
		// model->dump();
		
		double f = model->getProbabilityOfIndividual(&population[0]);
		
		cout << "p(0,1,2):" << pow(10, f) << endl;
		
		CPPUNIT_ASSERT( pow(10, f) == 1.0 );
	}
	
	#define ELL4 4
	void testProbability2()
	{
		cout << "testProbability2\n";
		
		model = new Model_EHBSA(ELL4);
		
		Chromosome* population = new Chromosome[6];
		
		
		int a[6][ELL4] = {{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1}};
		
		for(int i = 0; i < 5; i++)
		{
			population[i].init (ELL4);
			population[i].setGenes(a[i]);
		}
		
		model->buildModel(population, 5);
		model->dump();
		
		double f = model->getProbabilityOfIndividual(&population[0]);
		
		cout << "p(0,1,2,3):" << pow(10, f) << endl;
		
		CPPUNIT_ASSERT( pow(10, f) );
	}
	
	void testProbability3()
	{
		cout << "testProbability3\n";
		
		model = new Model_EHBSA(ELL4);
		
		Chromosome* population = new Chromosome[24];
		
		
		int a[24][ELL4] = {
			{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1},
			{1, 0, 2, 3}, {1, 0, 3, 2}, {1, 2, 0, 3}, {1, 2, 3, 0}, {1, 3, 0, 2}, {1, 3, 2, 0},
			{2, 0, 1, 3}, {2, 0, 3, 1}, {2, 1, 0, 3}, {2, 1, 3, 0}, {2, 3, 0, 1}, {2, 3, 1, 0},
			{3, 0, 1, 2}, {3, 0, 2, 1}, {3, 1, 0, 2}, {3, 1, 2, 0}, {3, 2, 0, 1}, {3, 2, 1, 0}
		};
		
		
		for(int i = 0; i < 24; i++)
		{
			population[i].init (ELL4);
			population[i].setGenes(a[i]);
		}
		
		 model->buildModel(population, 24);
		 model->dump();
		
		double f = model->getProbabilityOfIndividual(&population[0]);
		double f2 = model->getProbabilityOfIndividual(&population[23]);
		
		cout << "p(0,1,2,3):" << pow(10, f) << endl;
		cout << "p(3,2,1,0):" << pow(10, f2) << endl;
		
		CPPUNIT_ASSERT( pow(10, f) );
	}

	void testModelGain()
	{
	    cout << "Test E-Model Gain\n";

	    model = new Model_EHBSA(ELL4);

	    Chromosome* population = new Chromosome[24];
	    Chromosome* population2 = new Chromosome[24];

	    
	    int a[24][ELL4] = {
		{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1},
		{1, 0, 2, 3}, {1, 0, 3, 2}, {1, 2, 0, 3}, {1, 2, 3, 0}, {1, 3, 0, 2}, {1, 3, 2, 0},
		{2, 0, 1, 3}, {2, 0, 3, 1}, {2, 1, 0, 3}, {2, 1, 3, 0}, {2, 3, 0, 1}, {2, 3, 1, 0},
		{3, 0, 1, 2}, {3, 0, 2, 1}, {3, 1, 0, 2}, {3, 1, 2, 0}, {3, 2, 0, 1}, {3, 2, 1, 0}
	    };

	    int b[24][ELL4] = {
		{0, 1, 2, 3}, {0, 1, 2, 3}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1},
		{1, 0, 2, 3}, {1, 0, 3, 2}, {1, 2, 0, 3}, {1, 2, 3, 0}, {1, 3, 0, 2}, {1, 3, 2, 0},
		{2, 0, 1, 3}, {2, 0, 3, 1}, {2, 1, 0, 3}, {2, 1, 3, 0}, {2, 3, 0, 1}, {2, 3, 1, 0},
		{3, 0, 1, 2}, {3, 0, 2, 1}, {3, 1, 0, 2}, {3, 1, 2, 0}, {3, 2, 0, 1}, {3, 2, 1, 0}
	    };

	    int CROP = 6;

	    for(int i = 0; i < CROP; i++)
	    {
		population[i].init (ELL4);
		population[i].setGenes(a[i]);
		population2[i].init (ELL4);
		population2[i].setGenes(b[i]);
	    }
		 
	    model->buildModel(population, CROP);
	    double pM1 = model->getProbabilityOfModel(population, CROP);
	    model->dump();
	    double f1 = model->getProbabilityOfIndividual(&population[0]);
	    double v1 = model->getModelVariance();

	    model->buildModel(population2, CROP);
	    double pM2 = model->getProbabilityOfModel(population2, CROP);
	    model->dump();
	    double f2 = model->getProbabilityOfIndividual(&population[0]);
	    double v2 = model->getModelVariance();

	    cout << "pM1: " << pM1 << endl;
	    cout << "pM2: " << pM2 << endl;
	    cout << "Minimum Model Gain: " << pM2 - pM1 << endl;
	    cout << "p1: " << pow(10, f1) << endl;
	    cout << "p2: " << pow(10, f2) << endl;
	    cout << "individual gain: " << f2 - f1 << endl;
	    cout << "v1: " << v1 << endl;
	    cout << "v2: " << v2 << endl;
	    cout << "variance gain: " << v2 - v1 << endl;
	    cout << endl << endl;
 
	    delete[] population;
	    delete[] population2;
	}

	void testUpdate()
	{ 
	    cout << "Test E-Model Update\n";

	    model = new Model_EHBSA(ELL4);

	    Chromosome* population = new Chromosome[24];
	    Chromosome* population2 = new Chromosome[24];
	    int a[6][ELL4] = {{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1}};
	    int b[6][ELL4] = {{0, 1, 2, 3}, {0, 1, 2, 3}, {0, 2, 1, 3}, {0, 2, 3, 1}, {0, 3, 1, 2}, {0, 3, 2, 1}};
	
	    
	    int CROP = 6;

	    for(int i = 0; i < CROP; i++)
	    {
		population[i].init (ELL4);
		population[i].setGenes(a[i]);
		population2[i].init (ELL4);
		population2[i].setGenes(b[i]);
	    }
		 

	    model->buildModel(population2, CROP);
	    cout << "Target model:\n";
	    model->dump();
	    
	    model->buildModel(population, CROP);
	    cout << "Start model:\n";
	    model->dump();
	    
	    cout << "Updated model:\n";
	    model->update(&population[0], &population[1]);
	    model->dump();


	    delete[] population;
	    delete[] population2;
	    delete model;
	}
};
