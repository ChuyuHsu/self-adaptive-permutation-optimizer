/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>
#include <typeinfo>

#include "../../source/Array2D.h"
#include "../../source/chromosome.h"

using namespace std;

class TestArray2D : public CppUnit::TestFixture {
private:
	Array2D<int> *a2d;
public:
	TestArray2D() : a2d(NULL) {
	}

	virtual ~TestArray2D() {
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for FSSP_Evaluator");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestArray2D>("Test1 - Constructor.",
				&TestArray2D::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestArray2D>("Test2 - Attributes.",
				&TestArray2D::testAttributes ));

		suiteOfTests->addTest(new CppUnit::TestCaller<TestArray2D>("Test3 - Set & Read.",
				&TestArray2D::testSetGet ));	

		suiteOfTests->addTest(new CppUnit::TestCaller<TestArray2D>("Test4 - Inner Class.",
				&TestArray2D::testInnerClass ));

		suiteOfTests->addTest(new CppUnit::TestCaller<TestArray2D>("Test6 - double type.",
				&TestArray2D::testDoubleArray ));	

		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( a2d == NULL );
		
		a2d = new Array2D<int>();
		
		CPPUNIT_ASSERT( a2d != NULL );

	}
	
	void testAttributes()
	{
			printf("test Attributes\n");

			a2d = new Array2D<int>(20, 10);

			

			CPPUNIT_ASSERT( a2d->getWidth() == 10 );
			CPPUNIT_ASSERT( a2d->getHeight() == 20 );

			delete a2d;
	}

	void testInnerClass()
	{
			printf("test inner classes\n");
			Array2D<int> qq1(20, 10);
			std::cout << typeid(qq1[0]).name() << '\n';

			delete a2d;
	}
	
	void testSetGet() 
	{
		printf("test Set Get\n");

		a2d = new Array2D<int>(20, 10);

		(*a2d)[0][0] = 20*0+0;	

		CPPUNIT_ASSERT( (*a2d)[0][0] == 0 );

		(*a2d)[19][9] = 10*19+9;	

		printf("[19][9]:%d\n", (*a2d)[19][9]);

		CPPUNIT_ASSERT( (*a2d)[19][9] == 10*19+9 );

		for(int i = 0; i < 20; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				(*a2d)[i][j] = 10*i+j;
			}
		}


		for(int i = 0; i < 20; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				// printf("%d ", (*a2d)[i][j]);
				CPPUNIT_ASSERT( (*a2d)[i][j] == 10*i+j );
			}
		}		

		
		
		
		delete a2d;
	}
	

	void testDoubleArray() 
	{
		printf("testDoubleArray\n");

		Array2D<double> d2d(50, 10);

		for(int i = 0; i < 50; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				d2d[i][j] = (10*i+j) / 100.0;

				// printf("%f ", d2d[i][j]);
				double q = (10*i+j) / 100.0;
				CPPUNIT_ASSERT( d2d[i][j] == q );
			}
		}
	}	

};
