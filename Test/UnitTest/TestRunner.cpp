/*
 * TestRunner.cpp
 *
 *  Created on: Feb 16, 2013
 *      Author: Arvind A de Menezes Pereira
 */

#include <iostream>
#include <cppunit/TestSuite.h>
#include <cppunit/ui/text/TestRunner.h>

#include "TestFSSP_Evaluator.h"
#include "TestLOP_Evaluator.h"
#include "TestQAP_Evaluator.h"
#include "TestTSP_Evaluator.h"
#include "TestCVRP_Evaluator.h"
#include "TestArray2D.h"
#include "TestDice.h"
// #include "Test_model_EHBSA.h"
// #include "Test_model_NHBSA.h"

using namespace std;


int main() {
	CppUnit::TextUi::TestRunner runner;

	cout << "Creating Test Suites:" << endl;

	// cout << "TestArray2D added" << endl;
	// runner.addTest(TestArray2D::suite()); 

	cout << "Test CVRP added" << endl;
	runner.addTest(TestCVRP_Evaluator::suite());
	cout << endl;
    
	cout << "TestFSSP_Evaluator added" << endl;
	runner.addTest(TestFSSP_Evaluator::suite());
	cout << endl;	
	
	cout << "Test LOP added" << endl;
	runner.addTest(TestLOP_Evaluator::suite());
	cout << endl;
	
	cout << "Test LOP added" << endl;
	runner.addTest(TestQAP_Evaluator::suite());
	cout << endl;
	
	cout << "Test TSP added" << endl;
	runner.addTest(TestTSP_Evaluator::suite());
	cout << endl;

  cout << "Test Dice added" << endl;
  runner.addTest(TestDice::suite());
  cout << endl;
	
	cout<< "Running the unit tests."<<endl;
	runner.run();

	return 0;
}
