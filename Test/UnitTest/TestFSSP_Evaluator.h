/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>
#include <cstdlib>

#include "../../source/evaluator.h"
#include "../../source/chromosome.h"

using namespace std;

class TestFSSP_Evaluator : public CppUnit::TestFixture {
private:
	FSSP_Evaluator *testFSSP_Evaluator;
	string benchmark1, benchmark2, benchmark3, optima1, optima2, optima3;
public:
	TestFSSP_Evaluator() : testFSSP_Evaluator(NULL) {
		benchmark1 = "../../benchmarks/fssp/20_5_01_ta001.txt";
		optima1 = "fssp/1280.txt";
		benchmark2 = "../../benchmarks/fssp/5_2.txt";
		optima2 = "fssp/24.txt";
		benchmark3 = "../../benchmarks/fssp/5_3.txt";
		optima3 = "fssp/35.txt";
	}
	virtual ~TestFSSP_Evaluator() {
		delete testFSSP_Evaluator;
	}

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for FSSP_Evaluator");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestFSSP_Evaluator>("Test1 - Constructor.",
				&TestFSSP_Evaluator::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestFSSP_Evaluator>("Test2 - Attributes.",
				&TestFSSP_Evaluator::testAttributes ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestFSSP_Evaluator>("Test3 - Calculation1.",
				&TestFSSP_Evaluator::testCalculation1 ));
				
		suiteOfTests->addTest(new CppUnit::TestCaller<TestFSSP_Evaluator>("Test4 - Calculation2.",
				&TestFSSP_Evaluator::testCalculation2 ));
				
		// suiteOfTests->addTest(new CppUnit::TestCaller<TestFSSP_Evaluator>("Test5 - Calculation3.",
				// &TestFSSP_Evaluator::testCalculation3 ));

		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( testFSSP_Evaluator == NULL );
		
		testFSSP_Evaluator = new FSSP_Evaluator(benchmark1.c_str());
		
		CPPUNIT_ASSERT( testFSSP_Evaluator != NULL );
	}
	
	void testAttributes()
	{
			printf("test Attributes\n");
			testFSSP_Evaluator = new FSSP_Evaluator(benchmark1.c_str());
			
			CPPUNIT_ASSERT( testFSSP_Evaluator->getProblemSize() == 20 );
			CPPUNIT_ASSERT( testFSSP_Evaluator->getBest() == 1278.0 );
	}
	
	void testCalculation1() 
	{
		printf("test Calculation1\n");
		testFSSP_Evaluator = new FSSP_Evaluator(benchmark1.c_str());
		
		cout << "p-legnth:" << testFSSP_Evaluator->getProblemSize() << endl;
		
		Chromosome* c = new Chromosome(testFSSP_Evaluator->getProblemSize(), testFSSP_Evaluator);

		readSolution(c, optima1.c_str());
		
		cout << "fitness:" << c->getFitness() << endl;
		
		CPPUNIT_ASSERT( c->getFitness() == 1278 );

		delete c;
	}
	
	void testCalculation2() 
	{
		printf("test Calculation2\n");
		testFSSP_Evaluator = new FSSP_Evaluator(benchmark2.c_str());
		
		// cout << "p-legnth:" << testFSSP_Evaluator->getProblemSize() << endl;
		
		Chromosome* c = new Chromosome(testFSSP_Evaluator->getProblemSize(), testFSSP_Evaluator);

		readSolution(c, optima2.c_str());
		
		// cout << "fitness:" << c->getFitness() << endl;
		
		CPPUNIT_ASSERT( c->getFitness() == 24 );
		
		delete c;
	}
	
	void testCalculation3() 
	{
		printf("test Calculation3\n");
		testFSSP_Evaluator = new FSSP_Evaluator("../../benchmarks/flowshop/5_3.txt");
		
		Chromosome* c = new Chromosome(testFSSP_Evaluator->getProblemSize(), testFSSP_Evaluator);

		readSolution(c, "fssp/35.txt");
		
		cout << "fitness:" << c->getFitness() << endl;
		
		CPPUNIT_ASSERT( c->getFitness() == 35 );

		delete c;
	}

	void readSolution(Chromosome* c, const char* f)
	{
		ifstream fin(f);
   		if(!fin){
      			printf("ERROR: Can't find opt.txt\n");
     			CPPUNIT_ASSERT(false);
		}

		for(int i = 0; i < c->getLength(); i++)
		{
    			char j[256];
      			fin >> j;
				c->setVal(i, atoi(j)-1);
				// cout << "in: " << atoi(j)-1 << endl;
		}
		
		fin.close();
   	
	}
};
