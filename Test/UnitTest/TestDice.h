/*
 * TestLin2dSolver.cpp
 *
  *  Modified on: Oct 03, 2013
 *		Author Chewy Hsu 
 */

#include <iostream>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCase.h>
#include <fstream>
#include <string>
#include <typeinfo>

#include "../../source/dice.h"

using namespace std;

class TestDice : public CppUnit::TestFixture {
private:
	Dice *dice;
public:
	TestDice() : dice(NULL) {
	}

	virtual ~TestDice() {
	    delete dice;
  }

	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite("Test for Dice");

		suiteOfTests->addTest(new CppUnit::TestCaller<TestDice>("Test1 - Constructor.",
				&TestDice::testConstructor ));
	
		suiteOfTests->addTest(new CppUnit::TestCaller<TestDice>("Test2 - Set distribution.",
				&TestDice::testSetDist ));

		suiteOfTests->addTest(new CppUnit::TestCaller<TestDice>("Test3 - Sample",
				&TestDice::testSample ));	

		suiteOfTests->addTest(new CppUnit::TestCaller<TestDice>("Test4 - Sample Generalize",
				&TestDice::testSampleGeneralize ));	
		return suiteOfTests;
	}

	/// Setup method
	void setUp() {}

	/// Teardown method
	void tearDown() {}

protected:
	void testConstructor() 
	{
		printf("test Constructor\n");
		
		CPPUNIT_ASSERT( dice == NULL );
		
		dice = new Dice();
		
		CPPUNIT_ASSERT( dice != NULL );
	}
	
	void testSetDist()
	{
			printf("test set distribution\n");

      double distArray[] = {0.2, 0.4, 0.2, 0.2};
      std::vector<double> dist (distArray, distArray + sizeof(distArray)/sizeof(double));
			dice = new Dice(dist);
	}

	void testSample()
	{
			printf("test sample\n");
      double distArray[] = {0.2, 0.4, 0.2, 0.2};
      dice = new Dice(distArray, 4);

      int count[] = {0, 0, 0, 0};
      for(int i = 0; i < 1000; i++)
      {
          int u = dice->sample();
          count[u]++;
      }
    
      CPPUNIT_ASSERT( 350 < count[1] && count[1] < 450 );
			
	}
	
	void testSampleGeneralize()
	{
			printf("test sample generalize\n");
      double distArray[] = {2, 4, 2, 2};
      dice = new Dice(distArray, 4);

      int count[] = {0, 0, 0, 0};
      for(int i = 0; i < 1000; i++)
      {
          int u = dice->sample();
          count[u]++;
      }
    
      CPPUNIT_ASSERT( 350 < count[1] && count[1] < 450 );
			
	}
};
